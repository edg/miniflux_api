use crate::IconID;
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FavIcon {
    pub id: IconID,
    pub data: String,
    pub mime_type: String,
}

impl FavIcon {
    /// destroy this FavIcon and gain ownership of all the data it contains
    pub fn decompose(self) -> (IconID, String, String) {
        (self.id, self.data, self.mime_type)
    }
}
