use serde_derive::Deserialize;

/// Miniflux error struct
#[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
pub struct MinifluxError {
    /// Human readable error message
    pub error_message: String,
}
