use crate::models::EntryStatus;
use crate::MinifluxApi;
use reqwest::Client;
use std::env;
use url::Url;

fn test_setup_env() -> MinifluxApi {
    dotenv::dotenv().expect("Failed to read .env file");
    let url = env::var("MINIFLUX_URL").expect("Failed to read MINIFLUX_URL");
    let user = env::var("MINIFLUX_USER").expect("Failed to read MINIFLUX_USER");
    let pw = env::var("MINIFLUX_PW").expect("Failed to read MINIFLUX_PW");

    let url = Url::parse(&url).unwrap();
    MinifluxApi::new(&url, user, pw)
}

#[tokio::test(basic_scheduler)]
async fn healthcheck() {
    let api = test_setup_env();
    api.healthcheck(&Client::new()).await.unwrap();
}

#[tokio::test(basic_scheduler)]
async fn current_user() {
    let api = test_setup_env();
    let user = api.get_current_user(&Client::new()).await.unwrap();
    let username = env::var("MINIFLUX_USER").unwrap();
    assert_eq!(username, user.username);
}

#[tokio::test(basic_scheduler)]
async fn get_feeds() {
    let api = test_setup_env();
    let feeds = api.get_feeds(&Client::new()).await.unwrap();
    assert!(!feeds.is_empty())
}

#[tokio::test(basic_scheduler)]
async fn get_categories() {
    let api = test_setup_env();
    let categories = api.get_categories(&Client::new()).await.unwrap();
    assert!(!categories.is_empty())
}

#[tokio::test(basic_scheduler)]
async fn get_entries() {
    let api = test_setup_env();
    let entries = api
        .get_entries(
            Some(EntryStatus::Read),
            None,
            Some(5),
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            &Client::new(),
        )
        .await
        .unwrap();
    assert_eq!(5, entries.len());
}

#[tokio::test(basic_scheduler)]
async fn create_delete_feed() {
    let api = test_setup_env();
    let feed_url = Url::parse("https://feedforall.com/sample.xml").unwrap();

    let feed_id = api.create_feed(&feed_url, 2, &Client::new()).await.unwrap();
    let feed = api.get_feed(feed_id, &Client::new()).await.unwrap();
    assert_eq!(feed.feed_url, feed_url.as_str());

    api.delete_feed(feed_id, &Client::new()).await.unwrap();
    let error = api.get_feed(feed_id, &Client::new()).await;
    assert!(error.is_err());
}
